import {
    Meteor
} from 'meteor/meteor';
import {
    CreateUser,
    updateFullName,
    loginAPI
} from './methods.js';



Meteor.method("create-user", function (username, password, email, firstName, lastName, phoneId) {
    return CreateUser.call({
        email: username,
        password: password,
        firstName: email,
        lastName: firstName,
        username: lastName,
        phoneId: phoneId,
        api: true
    });
}, {
    url: "/create-user",
    httpMethod: "post",
    getArgsFromRequest: function (request) {
        var content = request.body;
        return [content.username, content.password, content.email, content.firstName, content.lastName, content.phoneId];
    }
});


Meteor.method("change-user-name", function (id, firstName, lastName) {
    return updateFullName.call({
        id: id,
        lastName: firstName,
        username: lastName,
    });
}, {
    url: "/change-user-name",
    httpMethod: "post",
    getArgsFromRequest: function (request) {
        var content = request.body;
        return [content.id, content.firstName, content.lastName];
    }
});

Meteor.method("login-api", function (username, password, phoneId) {
    console.log(arguments);
    return loginAPI.call({
        username: username,
        password: password,
        phoneId: phoneId
    });
}, {
    url: "/login/user",
    httpMethod: "post",
    getArgsFromRequest: function (request) {
        var content = request.body;
        return [content.username, content.password, content.phoneId];
    }
});

Meteor.method("logout-api", function (id, phoneId) {
    return logOut.call({
        id: id,
        phoneId: phoneId
    });
}, {
    url: "/logout",
    httpMethod: "post",
    getArgsFromRequest: function (request) {
        var content = request.body;
        return [content.id, content.phoneId];
    }
});