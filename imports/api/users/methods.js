import {
    Meteor
} from 'meteor/meteor';
import {
    ValidatedMethod
} from 'meteor/mdg:validated-method';
import {
    Accounts
} from 'meteor/accounts-base';

import {
    moment
} from 'meteor/momentjs:moment';
import SimpleSchema from 'simpl-schema';
import {
    Roles
} from 'meteor/alanning:roles';
import './users.js';

export const CreateUser = new ValidatedMethod({
    name: 'CreateUser',
    validate: new SimpleSchema({
        email: {
            type: String
        },
        password: {
            type: String
        },
        firstName: {
            type: String
        },
        lastName: {
            type: String
        },
        username: {
            type: String
        },
        phoneId: {
            type: String
        },
        api: {
            type: String
        }
    }).validator(),
    run(p) {
        try {

            if (Meteor.isServer) {
                var user = Accounts.createUser({
                    email: p.email,
                    username: p.username,
                    password: p.password,

                });
                console.log("user", user);
                var phones = [];
                phones.push(p.phoneId);
                Meteor.users.update({
                    _id: user
                }, {
                    $set: {
                        "profile.firstName": p.firstName,
                        "profile.lastName": p.lastName,
                        "profile.phoneId": phones
                    }
                });
                if (p.api === true) {
                    Roles.addUsersToRoles(user, 'user', "saaed");
                } else {
                    Roles.addUsersToRoles(user, 'admin', "saaed");
                }
                return Meteor.users.findOne({
                    _id: user
                });


            }


            return Meteor.users.findOne({
                username: p.username
            });
        } catch (error) {
            console.log(error);

            throw new Meteor.Error(001, error);
        }
    },
});

export const updateFullName = new ValidatedMethod({
    name: 'updateFullName',
    validate: new SimpleSchema({
        id: {
            type: String
        },
        firstName: {
            type: String
        },
        lastName: {
            type: String
        },
    }).validator(),
    run(p) {
        try {
            Meteor.users.update({
                _id: id
            }, {
                $set: {
                    "profile.firstName": p.firstName,
                    "profile.lastName": p.lastName,
                }
            })
            return Meteor.users.findOne({
                _id: p.id
            });
        } catch (error) {
            console.log(error);
            throw new Meteor.Error(002, error);
        }
    },
});

export const loginAPI = new ValidatedMethod({
    name: 'loginAPI',
    validate: new SimpleSchema({
        username: {
            type: String
        },
        password: {
            type: String
        },
        phoneId: {
            type: String
        }
    }).validator(),
    run(p) {
        try {
            if (Meteor.isServer) {
                var user = Accounts.findUserByUsername(p.username);

                if (!user && user === undefined) {
                    return {
                        "error": "username not found"
                    }
                }

                var user = Accounts.findUserByEmail(p.username);

                if (!user && user === undefined) {
                    return {
                        "error": "email not found"
                    }
                }
                var result = Accounts._checkPassword(user, options.password);
                console.log(result);
                if (!result || result === undefined) {
                    return {
                        "error": "wrong password"
                    }
                }

                if (result.error) {
                    return {
                        "error": "wrong password"
                    }
                }
                check(result, {
                    userId: String
                });


                var user = Meteor.users.findOne({
                    _id: result.userId
                });
                var id = result.userId;

                var stampedLoginToken = Accounts._generateStampedLoginToken();
                check(stampedLoginToken, {
                    token: String,
                    when: Date
                });

                Accounts._insertLoginToken(id, stampedLoginToken);
                var tokenExpiration = Accounts._tokenExpiration(stampedLoginToken.when);
            }
            return Meteor.users.findOne({
                username: p.username
            });
        } catch (error) {
            console.log(error);
            throw new Meteor.Error(002, error);
        }
    },
});

export const getUser = new ValidatedMethod({
    name: 'getUser',
    validate: new SimpleSchema({
        id: {
            type: String
        }
    }).validator(),
    run(p) {
        try {
            return Meteor.users.findOne({
                _id: p.id
            }, {
                fields: {
                    'profile': 1
                }
            })
        } catch (error) {
            console.log(error);
            throw new Meteor.Error(002, error);
        }
    },
});

export const getUsers = new ValidatedMethod({
    name: 'getUsers',
    validate: null,
    run(p) {
        try {
            return Meteor.users.find()
        } catch (error) {
            console.log(error);
            throw new Meteor.Error(002, error);
        }
    },
});


export const logOut = new ValidatedMethod({
    name: 'logOut',
    validate: new SimpleSchema({
        id: {
            type: String
        },
        phoneId: {
            type: String
        }
    }).validator(),
    run(p) {
        try {
            var user = Meteor.users.findOne({
                _id: p.id
            });
            if (user.profile) {
                var userPhones = user.profile.phoneId;
                for (var index = 0; index < userPhones.length; index++) {
                    var element = userPhones[index];
                    if (element === p.phoneId) {
                        userPhones.slice(index, 1)
                    }
                }

                return Meteor.users.update({
                    _id: id
                }, {
                    $set: {
                        "profile.phoneId": userPhones
                    }
                })
            }

        } catch (error) {
            console.log(error);
            throw new Meteor.Error(002, error);
        }
    },
});


export const deleteUser = new ValidatedMethod({
    name: 'deleteUser',
    validate: new SimpleSchema({
        id: {
            type: String
        }
    }).validator(),
    run(p) {
        try {
            return Meteor.users.remove({
                _id: p.id
            })
        } catch (error) {
            console.log(error);
            throw new Meteor.Error(002, error);
        }
    },
});