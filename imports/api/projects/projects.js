import {
    Meteor
} from 'meteor/meteor';
import {
    Mongo
} from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';

export const Projects = new Mongo.Collection('projects');

Schema = {};

Schema.Users = new SimpleSchema({
    id: {
        type: String,
        optional: true
    },
    amount: {
        type: String,
        optional: true
    }
});

Schema.Project = new SimpleSchema({
    name: {
        type: String,
        optional: true
    },
    description: {
        type: String,
        optional: true
    },
    adress: {
        type: String,
        optional: true
    },
    lng: {
        type: String,
        optional: true
    },
    lat: {
        type: String,
        optional: true
    },
    contactNumber: {
        type: String,
        optional: true
    },
    contactName: {
        type: String,
        optional: true
    },
    currentAmount: {
        type: String,
        optional: true
    },
    fullAmmount: {
        type: String,
        optional: true
    },
    type: {
        type: String,
        allowedValues: ['zakat', 'sadaka'],
        optional: true
    },
    users: {
        type: Array,
        // For accounts-password, either emails or username is required, but not both. It is OK to make this
        // optional here because the accounts-password package does its own validation.
        // Third-party login packages may not require either. Adjust this schema as necessary for your usage.
        optional: true
    },
    "users.$": {
        type: Schema.Users
    },
});


Projects.attachSchema(Schema.Project);