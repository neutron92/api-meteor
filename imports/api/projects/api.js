import {
    Meteor
} from 'meteor/meteor';
import {
    getProjects,
    getProject,
    addAmount,
    getMyContributions
} from './methods.js';



Meteor.method("get-projects", function (type) {
    return getProjects.call({
        type: type
    });
}, {
    url: "/get-projects",
    httpMethod: "post",
    getArgsFromRequest: function (request) {
        var content = request.body;
        return [content.type];
    }
});


Meteor.method("get-project", function (id) {
    return getProject.call({
        id: id
    });
}, {
    url: "/get-project",
    httpMethod: "post",
    getArgsFromRequest: function (request) {
        var content = request.body;
        return [content.id];
    }
});


Meteor.method("add-amount", function (id,amount,userId) {
    return addAmount.call({
        _id: id,
        amount:amount,
        userId:userId
    });
}, {
    url: "/add-amount",
    httpMethod: "post",
    getArgsFromRequest: function (request) {
        var content = request.body;
        return [content.id,content.amount,content.userId];
    }
});

Meteor.method("get-contributions", function (id) {
    return getMyContributions.call({
        id: id
    });
}, {
    url: "/get-contributions",
    httpMethod: "post",
    getArgsFromRequest: function (request) {
        var content = request.body;
        return [content.id];
    }
});