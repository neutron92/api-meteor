import {
    Meteor
} from 'meteor/meteor';
import {
    ValidatedMethod
} from 'meteor/mdg:validated-method';
import {
    Accounts
} from 'meteor/accounts-base';

import {
    moment
} from 'meteor/momentjs:moment';
import SimpleSchema from 'simpl-schema';
import {
    Roles
} from 'meteor/alanning:roles';
import {
    Projects
} from './projects.js';


export const addProject = new ValidatedMethod({
    name: 'addProject',
    validate: new SimpleSchema({
        name: {
            type: String,
            optional: true
        },
        description: {
            type: String,
            optional: true
        },
        adress: {
            type: String,
            optional: true
        },
        lng: {
            type: String,
            optional: true
        },
        lat: {
            type: String,
            optional: true
        },
        contactNumber: {
            type: String,
            optional: true
        },
        contactName: {
            type: String,
            optional: true
        },
        currentAmount: {
            type: String,
            optional: true
        },
        fullAmmount: {
            type: String,
            optional: true
        },
        type: {
            type: String,
            optional: true
        }
    }).validator(),
    run(p) {
        try {
            return Projects.insert({
                name: p.name,
                description: p.description,
                adress: p.adress,
                lng: p.lng,
                lat: p.lat,
                contactNumber: p.contactNumber,
                contactName: p.contactName,
                currentAmount: p.currentAmount,
                fullAmmount: p.fullAmmount,
                type: p.type,
            });
        } catch (error) {
            console.log(error);
            throw new Meteor.Error(003, error);
        }
    },
});

export const getProjects = new ValidatedMethod({
    name: 'getProjects',
    validate: new SimpleSchema({
        type: {
            type: String,
            optional: true
        }
    }).validator(),
    run(p) {
        try {
            return Projects.find({
                type: p.type,
            }).fetch();
        } catch (error) {
            console.log(error);
            throw new Meteor.Error(003, error);
        }
    },
});

export const getProject = new ValidatedMethod({
    name: 'getProject',
    validate: new SimpleSchema({
        id: {
            type: String,
            optional: true
        }
    }).validator(),
    run(p) {
        try {
            return Projects.findOne({
                _id: p.id,
            });
        } catch (error) {
            console.log(error);
            throw new Meteor.Error(003, error);
        }
    },
});


export const removeProject = new ValidatedMethod({
    name: 'getProjetc',
    validate: new SimpleSchema({
        id: {
            type: String,
            optional: true
        }
    }).validator(),
    run(p) {
        try {
            return Projects.remove({
                _id: p.id,
            });
        } catch (error) {
            console.log(error);
            throw new Meteor.Error(003, error);
        }
    },
});


export const editProject = new ValidatedMethod({
    name: 'editProject',
    validate: new SimpleSchema({
        _id: {
            type: String,
            optional: true
        },
        name: {
            type: String,
            optional: true
        },
        description: {
            type: String,
            optional: true
        },
        adress: {
            type: String,
            optional: true
        },
        lng: {
            type: String,
            optional: true
        },
        lat: {
            type: String,
            optional: true
        },
        contactNumber: {
            type: String,
            optional: true
        },
        contactName: {
            type: String,
            optional: true
        },
        currentAmount: {
            type: String,
            optional: true
        },
        fullAmmount: {
            type: String,
            optional: true
        },
        type: {
            type: String,
            optional: true
        }
    }).validator(),
    run(p) {
        try {
            return Projects.update({
                _id: _id
            }, {
                $set: {
                    name: p.name,
                    description: p.description,
                    adress: p.adress,
                    lng: p.lng,
                    lat: p.lat,
                    contactNumber: p.contactNumber,
                    contactName: p.contactName,
                    currentAmount: p.currentAmount,
                    fullAmmount: p.fullAmmount,
                    type: p.type,
                }
            });
        } catch (error) {
            console.log(error);
            throw new Meteor.Error(003, error);
        }
    },
});

export const addAmount = new ValidatedMethod({
    name: 'addAmount',
    validate: new SimpleSchema({
        _id: {
            type: String,
            optional: true
        },
        amount: {
            type: String,
            optional: true
        },
        userId: {
            type: String,
            optional: true
        }
    }).validator(),
    run(p) {
        try {
            var project = Projects.findOne({
                _id: p._id
            });
            var theAmount = Number(project.currentAmount) + Number(p.amount)
            var theUsers = project.users;
            theUsers.push({
                id: p.userId,
                amount: p.amount
            });
            Projects.update({
                _id: _id
            }, {
                $set: {
                    currentAmount: theAmount,
                    users: theUsers
                }
            });

            return Projects.findOne({
                _id: p._id
            });
        } catch (error) {
            console.log(error);
            throw new Meteor.Error(003, error);
        }
    },
});


export const getMyContributions = new ValidatedMethod({
    name: 'getMyContributions',
    validate: new SimpleSchema({
        id: {
            type: String,
            optional: true
        }
    }).validator(),
    run(p) {
        try {
            var projects = Projects.find({
                "users.id": p._id
            }).fetch();

            for (var index = 0; index < projects.length; index++) {
                var element = projects[index];
                var users = element.users;
                for (var index2 = 0; index2 < users.length; index2++) {
                    var element2 = users[index2];

                    if (element2.id === p.id) {
                        element.myAmmount = element2.amount;
                    }

                }

            }

            return projects;
        } catch (error) {
            console.log(error);
            throw new Meteor.Error(003, error);
        }
    },
});