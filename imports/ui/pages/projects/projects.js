import {
    Meteor
} from 'meteor/meteor';
import {
    Template
} from 'meteor/templating';
import {
    FlowRouter
} from 'meteor/kadira:flow-router';
import {
    TAPi18n
} from 'meteor/tap:i18n';
import {
    Bert
} from 'meteor/themeteorchef:bert';

import { pathFor } from 'meteor/arillo:flow-router-helpers';


import './projects.less';
import './projects.html';

Template.projects.onCreated(function() {
    this.regex = new ReactiveVar("");
});

Template.projects.onRendered(function() {

});
Template.projects.onDestroyed(function() {

});

Template.projects.helpers({
    myBots: function() {
        return []
    }
});
Template.projects.events({
    "click #add-new-bot": function(e, t) {
        e.preventDefault();
        var botId = Createbot.call({
                name: "newBot " + Meteor.user().profile.fullname,
                ownerId: Meteor.userId(),
                category: 'general',
                img: '/bot.png',
                description: "newBot Description"
            },
            function(err) {
                if (!err) {
                    Bert.alert({
                        type: 'success',
                        style: 'growl-top-right',
                        title: TAPi18n.__('botcreatedtitle'),
                        message: TAPi18n.__('botcreatedmsg'),
                        icon: 'fa-smile-o'
                    });
                    FlowRouter.go("new.bot", { id: botId });
                } else {
                    Bert.alert({
                        type: 'danger',
                        style: 'growl-top-right',
                        title: TAPi18n.__('error'),
                        message: TAPi18n.__(err.error + ""),
                        icon: 'fa-times'
                    });
                }

            }
        );
    },
    "keyup #search-input-bots": function(e, t) {
        //  e.preventDefault();

        var text = $("#search-input-bots").val();
        t.regex.set(text);
    },
    "keydown #search-input-bots": function(e, t) {
        //  e.preventDefault();
        var text = $("#search-input-bots").val();
        t.regex.set(text);
    },
    "change #search-input-bots": function(e, t) {
        // e.preventDefault();
        var text = $("#search-input-bots").val();
        t.regex.set(text);
    },
    "click #span-search-bots": function(e, t) {
        e.preventDefault();
        $("#search-input-bots").focus();
    },
});