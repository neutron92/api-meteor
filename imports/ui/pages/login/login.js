import {
    Meteor
} from 'meteor/meteor';
import {
    Template
} from 'meteor/templating';
import {
    FlowRouter
} from 'meteor/kadira:flow-router';
import {
    TAPi18n
} from 'meteor/tap:i18n';
import {
    Bert
} from 'meteor/themeteorchef:bert';

import './login.less';
import './login.html';

Template.login.onCreated(function() {});

Template.login.onRendered(function() {
    $('body').addClass('bg-gray');
    $("#login-form").validate({
        rules: {
            password: {
                minlength: 6
            }
        },
        messages: {
            email: {
                required: TAPi18n.__('emailrequired'),
                email: TAPi18n.__('validemail')
            },
            password: {
                required: TAPi18n.__('passrequired'),
                minlength: TAPi18n.__('validepass')
            }
        }
    })
});
Template.login.onDestroyed(function() {

});

Template.login.helpers({

});
Template.login.events({
    'submit #login-form': function(e, tpl) {
        e.preventDefault();
        let email = $("#email").val();
        let password = $("#password").val();
        Meteor.loginWithPassword(email, password, function(err) {
            if (!err) {
                Bert.alert({
                    type: 'info',
                    style: 'growl-top-right',
                    title: 'Bienvenue',
                    icon: 'fa-smile-o'
                });
                FlowRouter.go("/");
            } else {
                console.log(err);
                Bert.alert({
                    type: 'danger',
                    style: 'growl-top-right',
                    title: TAPi18n.__('error'),
                    message: TAPi18n.__(err.error + "login"),
                    icon: 'fa-times'
                });
            }
        });

    }
});